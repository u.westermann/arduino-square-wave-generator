#include <Arduino.h>
#include <avr/interrupt.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>


struct timer4_cfg
{
    float frequency;
    float abs_error;
    uint8_t ocr4c;
    uint8_t cs4;
};


static void execute_commands(void);
static void handle_help(void);
static void handle_set_autostop_time(void);
static void handle_set_autostop_count(void);
static void handle_set_frequency(void);
static void handle_toggle_edge(void);
static void handle_start(void);
static void timers_stop(void);
static void handle_stop(void);
static void handle_print_status(void);
static void timers_start(void);
static struct timer4_cfg timer_calc(float requested_frequency);

static const int LED_PIN = 13;
static uint32_t autostop_timeout = 0;
static uint32_t autostop_count = 0;
static volatile uint32_t stop_time = 0;
static volatile uint32_t last_run_time = 0;
static float user_frequency = 2000000.0;
static volatile bool running = false;
static struct timer4_cfg timer4_cfg;
static bool rising_edge = true;
static volatile uint32_t timer1_ovf_num = 0;


int main(void)
{
    init();

    USBDevice.attach();

    setup();

    while (1)
    {
        loop();
    }

    return 0;
}


void setup(void)
{
    Serial.begin(57600);

    // green LED shows running state
    pinMode(LED_PIN, OUTPUT);

    // calculate timer4 parameters
    timer4_cfg = timer_calc(user_frequency);

    // reset timer1, which is configured to PWM by default
    TCCR1A = 0;
    TCCR1B = 0;

    // set timer1 pin (PD6) as input
    DDRD &= ~(1 << DDD6); // pin PD6 as input

    // set timer4 pin (PB6) low
    DDRB |= (1 << DDB6); // pin PB6 as output
    PORTB &= ~(1 << PB6); // pin PB6 low
}


void loop(void)
{
    execute_commands();

    if (running && autostop_timeout != 0 && micros() >= stop_time)
    {
        handle_stop();
    }
}


static void execute_commands(void)
{
    if (Serial.available())
    {
        int c = Serial.read();
        Serial.print((char)c);

        switch (c)
        {
            case '?':
                handle_help();
                break;
            case 't':
                handle_set_autostop_time();
                break;
            case 'c':
                handle_set_autostop_count();
                break;
            case 'f':
                handle_set_frequency();
                break;
            case 'e':
                handle_toggle_edge();
                break;
            case 's':
                handle_start();
                break;
            case 'o':
                handle_stop();
                break;
            case 'p':
                handle_print_status();
                break;
            default:
                Serial.println("\r\nunknown command. type '?' for help.");
        }
    }
}


static void handle_help(void)
{
    Serial.println(" help: generate (pin D10) and count (pin D12) digital pulses.");
    delay(1);
    Serial.println("  t value: set approx. autostop timeout [us]. 0 = no autostop. default = 0.");
    delay(1);
    Serial.println("  c value: set approx. autostop count [edges]. 0 = no autostop. default = 0.");
    delay(1);
    Serial.println("  f value: set frequency [hz]. max = 8000000.0, min = 369.09, default = 2000000.0.");
    delay(1);
    Serial.println("  e:       toggle rising/falling edge detection for counter. default = rising.");
    delay(1);
    Serial.println("  s:       start frequency generation for t milliseconds.");
    delay(1);
    Serial.println("  o:       stop frequency generation.");
    delay(1);
    Serial.println("  p:       print status and edge count since last start.");
    delay(1);
    Serial.println("  ?:       this help.");
}


static void handle_set_autostop_time(void)
{
    if (running)
    {
        Serial.println("\r\n  not possible while running.");
        return;
    }

    String line = "";
    int c;

    while (c = Serial.read(), (c != '\r' && c != '\n' ))
    {
        if (c != -1)
        {
            Serial.print((char)c);
            line += (char)c;
        }
    }

    autostop_timeout = strtoul(line.c_str(), NULL, 10);

    Serial.print("\r\n  autostop_timeout = ");
    Serial.print(autostop_timeout);
    Serial.println(" us");
}


static void handle_set_autostop_count(void)
{
    if (running)
    {
        Serial.println("\r\n  not possible while running.");
        return;
    }

    String line = "";
    int c;

    while (c = Serial.read(), (c != '\r' && c != '\n' ))
    {
        if (c != -1)
        {
            Serial.print((char)c);
            line += (char)c;
        }
    }

    autostop_count = strtoul(line.c_str(), NULL, 10);

    autostop_count = autostop_count == 1 ? 2 : autostop_count;

    Serial.print("\r\n  autostop_count = ");
    Serial.print(autostop_count);
    Serial.println(" edges");
}


static void handle_set_frequency(void)
{
    if (running)
    {
        Serial.println("\r\n  not possible while running.");
        return;
    }

    String line = "";
    int c;

    while (c = Serial.read(), (c != '\r' && c != '\n' ))
    {
        if (c != -1)
        {
            Serial.print((char)c);
            line += (char)c;
        }
    }

    user_frequency = atof(line.c_str());
    timer4_cfg = timer_calc(user_frequency);

    Serial.print("\r\n  frequency = ");
    Serial.print(timer4_cfg.frequency);
    Serial.println(" hz");
}


static void handle_toggle_edge(void)
{
    if (running)
    {
        Serial.println("\r\n  not possible while running.");
        return;
    }

    rising_edge = !rising_edge;

    Serial.print("\r\n  rising_edge = ");
    Serial.print(rising_edge);

    if (rising_edge)
    {
        Serial.println(", activating pull-down on counter input");
        pinMode(12, INPUT);
    }
    else
    {
        Serial.println(", activating pull-up on counter input");
        pinMode(12, INPUT_PULLUP);
    }
}


static void handle_start(void)
{
    if (running)
    {
        Serial.println("\r\n  not possible while running.");
        return;
    }

    Serial.print("\r\n  start with frequency = ");
    Serial.print(timer4_cfg.frequency);
    Serial.println(" hz");

    timers_start();

    digitalWrite(LED_PIN, HIGH);
}


static void handle_stop(void)
{
    timers_stop();

    digitalWrite(LED_PIN, LOW);

    Serial.println("  stopped");
}


static void handle_print_status(void)
{
    Serial.print("\r\n  status:\r\n    running = ");
    Serial.print(running);
    Serial.print("\r\n    autostop_timeout = ");
    Serial.print(autostop_timeout);
    Serial.print(" us\r\n    autostop_count = ");
    Serial.print(autostop_count);
    Serial.print(" edges\r\n    last_run_time = ");
    Serial.print(last_run_time);
    Serial.print(" us\r\n    frequency = ");
    Serial.print(timer4_cfg.frequency);
    Serial.print(" hz\r\n    rising_edge = ");
    Serial.print(rising_edge);
    Serial.print("\r\n    edge_count = ");
    Serial.println(TCNT1 + timer1_ovf_num * 65536);
}


static void timers_start(void)
{
    cli();

    // timer 1 for frequency counting
    TCNT1 = 0; // clear timer counter
    TIMSK1 = (1 << TOIE1); // enable overflow interrupt

    if (autostop_count != 0 && autostop_count < 65535)
    {
        OCR1A = autostop_count - 1;
        TIMSK1 |= (1 << OCIE1A); // enable compare A match interrupt
    }

    if (rising_edge)
    {
        TCCR1B = (1 << CS12) | (1 << CS11) | (1 < CS10);
    }
    else
    {
        TCCR1B = (1 << CS12) | (1 << CS11);
    }

    TIFR1 = 0xff; // clear interrupt flags

    timer1_ovf_num = 0;

    // timer 4 for frequency generation
    PLLFRQ |= (1 << PLLTM0); // enable PLL postscaler for high speed timer
    TCNT4 = 0; // set timer value to 0
    OCR4C = timer4_cfg.ocr4c; // timer top value
    TCCR4A = (1 << COM4B0); // comparator B, toggle OC4B pin on compare match
    TCCR4B = timer4_cfg.cs4; // prescaler

    running = true;
    stop_time = micros() + autostop_timeout;

    sei();
}


static void timers_stop(void)
{
    cli();

    // timer 4
    TCCR4B = 0; // disable timer input clock 
    TCCR4A = 0; // normal port pin operation

    // timer 1
    TCCR1B = 0; // disable timer input clock
    TIMSK1 = 0; // disable timer interrupts

    last_run_time = micros() - (stop_time - autostop_timeout);

    running = false;

    sei();
}


static struct timer4_cfg timer_calc(float requested_frequency)
{
    static struct timer4_cfg timer[15];
    const float CK = 24000000.0;

    for (int i = 0; i < 15; i++)
    {
        timer[i].cs4 = i + 1; // 1..15
        uint16_t prescaler = pow(2, timer[i].cs4 - 1);

        float output_compare = roundf(CK / (requested_frequency * prescaler));

        output_compare = (output_compare < 3) ? 3 : output_compare;
        output_compare = (output_compare > 255) ? 255 : output_compare;

        timer[i].ocr4c = output_compare;
        timer[i].frequency = CK / (timer[i].ocr4c * prescaler);
        timer[i].abs_error = fabsf(timer[i].frequency - requested_frequency);
    }

    int index_best_match = 0;
    float smallest_abs_error = timer[0].abs_error;

    for (int i = 1; i < 15; i++)
    {
        if (timer[i].abs_error < smallest_abs_error)
        {
            index_best_match = i;
            smallest_abs_error = timer[i].abs_error;
        }
    }

    return timer[index_best_match];
}


ISR(TIMER1_OVF_vect)
{
    timer1_ovf_num += 1;

    if (autostop_count)
    {
        if (autostop_count > 0xffff)
        {
            uint32_t remaining_counts = autostop_count - 1 - timer1_ovf_num * 0x10000;

            if (remaining_counts <= 0xffff)
            {
                OCR1A = remaining_counts;
                TIMSK1 |= (1 << OCIE1A); // enable compare A match interrupt
                TIFR1 = (1 << OCF1A); // clear compare interrupt flag
            }
        }
    }
}


ISR(TIMER1_COMPA_vect)
{
    TCCR4B = 0; // disable timer input clock
    handle_stop();
}
