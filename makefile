ARDUINO_DIR = /home/ulf/Programs/arduino-1.8.12
ARDUINO_SRC = wiring_pulse.S WInterrupts.c hooks.c wiring.c wiring_analog.c wiring_digital.c wiring_pulse.c wiring_shift.c CDC.cpp HardwareSerial.cpp IPAddress.cpp PluggableUSB.cpp Print.cpp Stream.cpp Tone.cpp USBCore.cpp WMath.cpp WString.cpp abi.cpp new.cpp

SRCS = $(addprefix $(ARDUINO_DIR)/hardware/arduino/avr/cores/arduino/, $(ARDUINO_SRC)) main.cpp

BIN_DIR = bin
TMP_DIR = tmp
DOC_DIR = doc

TARGET = $(BIN_DIR)/main.hex

AS = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-as
CC = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-gcc
CXX = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-g++
RM = rm -f
LINKER = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-g++
OBJCOPY = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-objcopy
SIZE = $(ARDUINO_DIR)/hardware/tools/avr/bin/avr-size
FORMAT = clang-format -i -style=file
MKDIR = mkdir -p

OBJS = $(SRCS:%=$(TMP_DIR)/%.o)
DEPS = $(SRCS:%=$(TMP_DIR)/%.d)

MCU = atmega32u4
ASFLAGS =
CPPFLAGS = -MMD -I$(ARDUINO_DIR)/hardware/arduino/avr/cores/arduino/ -I$(ARDUINO_DIR)/hardware/arduino/avr/variants/micro -I$(ARDUINO_DIR)/hardware/tools/avr/avr/include -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MICRO -DARDUINO_ARCH_AVR -DUSB_VID=0x2341 -DUSB_PID=0x8037
CFLAGS = -std=gnu11 -ggdb3 -Wall -Wextra -Wpedantic -fno-exceptions -ffunction-sections -fdata-sections -mmcu=$(MCU)
CXXFLAGS = -std=gnu++17 -ggdb3 -Wall -Wextra -Wpedantic -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -mmcu=$(MCU)
LDFLAGS = -Wl,--gc-sections -mmcu=$(MCU)
LDLIBS =

ifdef release
	CFLAGS += -O$(release)
	CXXFLAGS += -O$(release)
else
	CFLGAS += -Og
	CXXFLAGS += -Og
endif

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJS)
	@echo LINKING: $<
	$(MKDIR) $(@D)
	$(LINKER) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $(basename $@).elf
	$(OBJCOPY) -O ihex -R .eeprom $(basename $@).elf $@
	$(SIZE) --format=avr --mcu=$(MCU) $(basename $@).elf

$(TMP_DIR)/%.c.o: %.c
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(TMP_DIR)/%.cpp.o: %.cpp
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(TMP_DIR)/%.S.o: %.S
	@echo COMPILING: $<
	$(MKDIR) $(@D)
	$(AS) $(ASFLAGS) -c $< -o $@

.PHONY: program
program: all
	# use bootloader
	./reset.py /dev/ttyACM0
	$(ARDUINO_DIR)/hardware/tools/avr/bin/avrdude -C$(ARDUINO_DIR)/hardware/tools/avr/etc/avrdude.conf -v -p $(MCU) -c avr109 -P /dev/ttyACM0 -U flash:w:$(TARGET)

	# use isp programmer
	#$(ARDUINO_DIR)/hardware/tools/avr/bin/avrdude -C$(ARDUINO_DIR)/hardware/tools/avr/etc/avrdude.conf -v -p $(MCU) -c avrispmkII -U flash:w:$(TARGET)

.PHONY: clean
clean:
	@echo CLEANING
	$(RM) -r *.o *.d $(BIN_DIR) $(TMP_DIR) $(DOC_DIR) compile_commands.json tags

.PHONY: format
format:
	clang-format -i -style=file $(shell find . -iname '*.c' -or -iname '*.cpp' -or -iname '*.h')

.PHONY: check
check:
	@echo CPPCHECK
	cppcheck main.cpp
	@echo CLANG-TIDY
	clang-tidy main.cpp

.PHONY: doc
doc:
	@echo GENERATING DOCUMENTATION
	mkdir -p $(DOC_DIR)
	doxygen

.PHONY: tags
tags:
	@echo GENERATING TAGS
	ctags -R . $(ARDUINO_DIR)

.PHONY: bear
bear:
	make clean
	bear make

-include $(DEPS)
